# Digital Design and Development
## Portfolio Task 3: A Single Page Application in Angular.js

### Overview

For this task, you will create a simple Angular.js web application using the Angular CLI and
based around steps 1-6 of the Angular tutorial on Tour of Heroes (https://angular.io/tutorial)

### Task Description

Begin by aiming for a working implementation of the Heroes example. Then, refactor the app
for your own collection. As stretch goals, consider extension of the app to include:-
 - Images
 - an HTTP backend
Ensure that you document, through code commenting, the work you have done to customise
the app.

### Submission

Deploy your app to UWE or Azure and submit the link as submission text. Also commit your
app to GitLab and submit the code.

import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import 'hammerjs';    //adding supporting guestures

import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; //add browser animation module

import {MatInputModule,MatListModule,MatSidenavModule,MatToolbarModule,MatIconModule,MatCardModule,MatButtonModule,MatOptionModule,MatSelectModule,MatChipsModule} from '@angular/material';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './services/in-memory-data.service';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
import { HeroesComponent }      from './heroes/heroes.component';
import { HeroSearchComponent }  from './hero-search/hero-search.component';
import { HeroService }          from './services/hero.service';
import { MessageService }       from './services/message.service';
import { MessagesComponent }    from './messages/messages.component';
import { SidebarService } from './services/sidebar.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FlexLayoutModule } from '@angular/flex-layout';  //flex layout is cool to make it responsive
import { NgDraggableModule } from 'angular-draggable'; 

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatInputModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    MatChipsModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatOptionModule,
    MatCardModule,
    NgDraggableModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroesComponent,
    HeroDetailComponent,
    MessagesComponent,
    HeroSearchComponent,
    SidebarComponent
  ],
  providers: [ HeroService, MessageService, SidebarService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

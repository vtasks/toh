export class Hero {
  id: number;
  name: string;
  type: string;
  details: string;
}
/* An new interface for the race*/
export class Race {
  name: string;
}
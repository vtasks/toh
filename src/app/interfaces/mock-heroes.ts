import { Hero, Race } from './hero';

/*Lets use my fav DD charactes, new fields added here are type and details.*/
export const HEROES: Hero[] = [
  { id: 1, name: 'Mr. Nice' , type: "Aarakocra" ,details: "Goblins occupy an uneasy place in a dangerous world, and they react by lashing out at any creatures they believe they can bully."},
  { id: 2, name: 'Narco'  , type: "Aasimar" ,details: "Aasimar are placed in the world to serve as guardians of law and good. Their patrons expect them to strike at evil, lead by example, and further the cause of justice."},
  { id: 3, name: 'Bombasto'  , type: "Bugbear",details: "Bugbears feature in the nightmare tales of many races — great, hairy beasts that creep through the shadows as quiet as cats."},
  { id: 4, name: 'Celeritas'  , type: "Dragonborn",details: "Dragonborn look very much like dragons standing erect in humanoid form, though they lack wings or a tail."},
  { id: 5, name: 'Magneta'  , type: "Dwarf",details: "Bold and hardy, dwarves are known as skilled warriors, miners, and workers of stone and metal."},
  { id: 6, name: 'RubberMan'  , type: "Elf",details: "Elves are a magical people of otherworldly grace, living in the world but not entirely part of it."},
  { id: 7, name: 'Dynama'  , type: "Feral Tiefling",details: "To be greeted with stares and whispers, to suffer violence and insult on the street, to see mistrust and fear in every eye: this is the lot of the tiefling."},
  { id: 8, name: 'Dr IQ' , type: "Firbolg",details: "Firbolg tribes cloister in remote forest strongholds, preferring to spend their days in quiet harmony with the woods."},
  { id: 9, name: 'Magma'  , type: "Genasi",details: "Genasi carry the power of the elemental planes of air, earth, fire, and water in their blood."},
  { id: 10, name: 'Moros'  , type: "Gnome",details: "A gnome’s energy and enthusiasm for living shines through every inch of his or her tiny body."},
  { id: 11, name: 'Asgard'  , type: "Goblin",details: "Goblins occupy an uneasy place in a dangerous world, and they react by lashing out at any creatures they believe they can bully."},
  { id: 12, name: 'Freyr'  , type: "Goliath",details: "Strong and reclusive, every day brings a new challenge to a goliath."},
  { id: 13, name: 'Kvasir'  , type: "Half-Elf",details: "Half-elves combine what some say are the best qualities of their elf and human parents."},
  { id: 14, name: 'Amonet'  , type: "Halfling",details: "The diminutive halflings survive in a world full of larger creatures by avoiding notice or, barring that, avoiding offense."},
  { id: 15, name: 'Anubis'  , type: "Half-Orc",details: "Half-orcs’ grayish pigmentation, sloping foreheads, jutting jaws, prominent teeth, and towering builds make their orcish heritage plain for all to see."},
  { id: 16, name: 'Apophis'  , type: "Hobgoblin",details: "War is the lifeblood of hobgoblins. Its glories are the dreams that inspire them. Its horrors don’t feature in their nightmares."},
  { id: 17, name: 'Ancients'  , type: "Human",details: "Humans are the most adaptable and ambitious people among the common races. Whatever drives them, humans are the innovators, the achievers, and the pioneers of the worlds."},
  { id: 18, name: 'Camulus'  , type: "Kenku",details: "Haunted by an ancient crime that robbed them of their wings, the kenku wander the world as vagabonds and burglars who live at the edge of human society."},
  { id: 16, name: 'Cronus'  , type: "Kobold",details: "Kobolds are typically timid and shy away from conflict, but they are dangerous and vicious if cornered."},
  { id: 20, name: 'Hathor'  , type: "Lizardfolk",details: "Lizardfolk possess an alien and inscrutable mindset, their desires and thoughts driven by a different set of basic principles than those of warm-blooded creatures."},
  { id: 21, name: 'Klorel'  , type: "Orc",details: "Orcs live a life that has no place for weakness, and every warrior must be strong enough to take what is needed by force."},
  { id: 22, name: 'Sokar'  , type: "Tabaxi",details: "Hailing from a strange and distant land, wandering tabaxi are catlike humanoids driven by curiosity to collect interesting artifacts, gather tales and stories, and lay eyes on all the world’s wonders."},
  { id: 23, name: 'Osiris'  , type: "Tiefling",details: "To be greeted with stares and whispers, to suffer violence and insult on the street, to see mistrust and fear in every eye: this is the lot of the tiefling."},
  { id: 24, name: 'Tanith'  , type: "Tortle",details: "What many tortles consider a simple life, others might call a life of adventure. They are nomad survivalists eager to explore the wilderness."},
  { id: 25, name: 'Gerak'  , type: "Triton",details: "Long-established guardians of the deep ocean floor, in recent years the noble tritons have become increasingly active in the world above."},
  { id: 26, name: 'Haikon'  , type: "Yuan-ti Pureblood",details: "The serpent creatures known as yuan-ti are all that remains of an ancient, decadent human empire."}
];
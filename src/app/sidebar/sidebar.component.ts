import { Component } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { SidebarService } from '../services/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  animations: [
    trigger(
        'slide', [
            state('collapse', style({
                width: 0,
                opacity: 0
            })),
            state('thin', style({
                width: '100%',
                opacity: 1
            })),
            transition('collapse => thin', animate('100ms ease-in')),
            transition('thin => collapse', animate('100ms ease-out'))
        ]
    ),
    trigger(
        'reveal', [
            state('collapse', style({
                width: 0,
                opacity: 0,
                'margin-left': 0
            })),
            state('thin', style({
                width: 0,
                opacity: 0,
                'margin-left': 0
            })),
            transition('thin => collapse', animate('100ms ease-in')),
            transition('collapse => thin', animate('100ms ease-out'))
        ]
    )
  ]
})

export class SidebarComponent {

  state = 'min';
  constructor(public sidepanel: SidebarService) {
      sidepanel.state.subscribe(s => {
          this.state = s;
      });
  }

}

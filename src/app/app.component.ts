import { Component, OnInit } from '@angular/core';
import { SidebarService } from './services/sidebar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tour of Heroes';

  dark = false;
  navItems = [
    {name: 'Dashboard', route: '/dashboard'},
    {name: 'Heroes', route: '/heroes'}
  ];

  constructor(public sidepanel: SidebarService) {}
  

}

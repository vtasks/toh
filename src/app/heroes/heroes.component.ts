import { Component, OnInit } from '@angular/core';

import { Hero, Race } from '../interfaces/hero';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];
  races: Race[];
  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
    this.getRaces();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
    .subscribe(heroes => this.heroes = heroes);
  }

  getRaces(): void {
    this.races = this.heroService.getRaces()
  }

  add(name: string,type: string,details: string): void {
    name = name.trim();
     //add race and details 
    type = type.trim();  
    details = details.trim();

    if (!name) { return; }
    this.heroService.addHero({ name, type, details } as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }

  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);
    this.heroService.deleteHero(hero).subscribe();
  }

}
